# Aplicação para novos desenvolvedores - Klicksite

Olá, se você chegou até aqui, provavelmente se interessou por uma de nossas vagas em aberto para desenvolvedores Backend.

Nosso processo de avaliação é formulado para diagnosticar o grau de conhecimento em assuntos importantes do nosso ponto de vista,
sendo constituido de um questionário simples com algumas questões sobre conhecimento técnico, e um teste prático de desenvolvimento.

# Como fazer sua aplicação

Para se candidatar é simples:

+ Faça um fork deste repositório;

+ Crie um branch com o seu nome;

+ Preencha o questionário que está em QUESTIONS.md;

+ Para o teste prático, siga as instruções em TEST.md;

+ Envie um Pull Request utlizando o branch que você criou através do bitbucket;

Boa sorte!
